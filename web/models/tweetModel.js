var mongoose = require('mongoose');
Schema = mongoose.Schema();

var tweetModel = new Schema({
    user: {
        name: {
            type: String
        },
        screen_name: {
            type: String
        }
    },
    text: {
        type: String
    }
});

module.exports= mongoose.model('tweet', tweetModel);