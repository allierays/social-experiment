var gulp = require('gulp'),
    nodemon = require('gulp-nodemon');


gulp.task('default', function(){
    nodemon({
        script: 'app.js',
        ext: 'js',
        env: {
            PORT:8000
        },
        ignore: ['./node_modules/**']
    })
    .on('restart', function(){
        console.log('Restarting');
    });
});


// Nodemon watches files and will automatically restart your node application if any files change. https://github.com/remy/nodemon
//wrap it in a gulp function and now you can just run gulp to watch for changes

